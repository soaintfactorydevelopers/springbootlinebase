package com.soaint.lineabase.commons.domains.request;

import com.soaint.lineabase.commons.domains.generic.ImplementadorDTO;

import java.util.HashSet;

public class ImplementadorDTORequest extends ImplementadorDTO {

    private Double salario;
    private HashSet<String> actividades;

}
