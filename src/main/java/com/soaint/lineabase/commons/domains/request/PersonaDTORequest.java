package com.soaint.lineabase.commons.domains.request;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersonaDTORequest extends PersonaDTO {

}
